#!/usr/bin/env python
# coding: utf-8

# In[17]:


import pickle
import pandas as pd
import numpy as np




# In[30]:


year = 2021
month = 4

input_file = f'https://nyc-tlc.s3.amazonaws.com/trip+data/fhv_tripdata_{year:04d}-{month:02d}.parquet'
output_file = f'output/fhv_tripdata_{year:04d}-{month:02d}.parquet'

with open('lin_reg.bin', 'rb') as f_in:
    dv, model = pickle.load(f_in)

    


# In[31]:



categorical = ['PUlocationID', 'DOlocationID']

def read_data(filename):
    df = pd.read_parquet(filename)
    
    df['duration'] = df.dropOff_datetime - df.pickup_datetime
    df['duration'] = df.duration.dt.total_seconds() / 60

    df = df[(df.duration >= 1) & (df.duration <= 60)].copy()

    df[categorical] = df[categorical].fillna(-1).astype('int').astype('str')
    df['ride_id'] = f'{2021:04d}/{2:02d}_' + df.index.astype('str')

    
    return df


# In[29]:


def apply_model(input_file=input_file, output_file=output_file):
    df = read_data(input_file)
    dicts = df[categorical].to_dict(orient='records')
    X_val = dv.transform(dicts)

    y_pred = model.predict(X_val)


    df_result = pd.DataFrame()
    df_result['ride_id'] = df['ride_id']
    df_result['predicted_duration'] = y_pred    
    
    df_result.to_parquet(output_file,
    engine='pyarrow',
    compression=None,
    index=False)

    print(y_pred.mean())


# In[27]:


apply_model(input_file=input_file, output_file=output_file)


# In[ ]:




